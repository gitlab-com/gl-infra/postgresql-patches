apt-get update
apt-get install -y build-essential \
  libreadline-dev \
  zlib1g-dev \
  flex \
  bison \
  libxml2-dev \
  libxslt-dev \
  libssl-dev \
  libxml2-utils \
  xsltproc \
  dpkg-dev \
  curl \
  git \
  debhelper \
  dh-exec \
  docbook-xml \
  docbook-xsl \
  gettext \
  krb5-multidev \
  libedit-dev \
  libipc-run-perl \
  libldap2-dev \
  libpam0g-dev \
  libperl-dev \
  libselinux1-dev \
  libsystemd-dev \
  pkg-config \
  python3-dev \
  systemtap-sdt-dev \
  tcl-dev \
  uuid-dev \
  clang-6.0 \
  llvm-6.0

PGVER=12.7
curl -O https://ftp.postgresql.org/pub/source/v$PGVER/postgresql-$PGVER.tar.gz
tar -zxvf postgresql-$PGVER.tar.gz

git clone -b debian/12.7-1 https://salsa.debian.org/postgresql/postgresql.git
cp -rf postgresql/debian postgresql-$PGVER

cd postgresql-$PGVER

echo 9 > debian/compat
cp ../patches/postgresql/55-subtrans-slru-pages-increase.patch debian/patches
echo "55-subtrans-slru-pages-increase.patch" >> debian/patches/series
# pgdg builds replace appropriate clang/llvm version:
# https://git.postgresql.org/gitweb/?p=pgapt.git;a=blob;f=jenkins/generate-pgdg-source;h=db9c80bf6448bd86c165d3c7a68669327ae28e93;hb=HEAD#l166
patch -p1 < ../patches/debian.patch
# Enable debug builds
sed -i -e "s/#dbg#//" debian/control

DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -rfakeroot -b -uc -us
